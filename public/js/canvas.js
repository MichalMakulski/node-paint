var CANVAS = (function(){
  var canvas = document.querySelector('canvas');
  var ctx = canvas.getContext('2d');
	var isDrawing, points = [ ];

	function onmousedown(e) {
		canvas.addEventListener('mousemove', onmousemove, false);
		points = [ ];	
		isDrawing = true;
		points.push({ x: e.clientX, y: e.clientY });
	};

	function onmousemove(e) {
		if (!isDrawing) return;
			points.push({ x: e.clientX, y: e.clientY });
			ctx.beginPath();
			ctx.moveTo(points[points.length - 2].x, points[points.length - 2].y);
			ctx.lineTo(points[points.length - 1].x, points[points.length - 1].y);
			ctx.stroke();
		for (var i = 0, len = points.length; i < len; i++) {
				dx = points[i].x - points[points.length-1].x;
				dy = points[i].y - points[points.length-1].y;
				d = dx * dx + dy * dy;
			if (d < 1000) {
				ctx.beginPath();
				ctx.strokeStyle = hexToRgbA(document.getElementById('color').value);
				ctx.moveTo( points[points.length-1].x + (dx * 0.2), points[points.length-1].y + (dy * 0.2));
				ctx.lineTo( points[i].x - (dx * 0.2), points[i].y - (dy * 0.2));
				ctx.stroke();
			}
		}
	};

	function onmouseup(e) {
		canvas.removeEventListener('mousemove', onmousemove, false);
		isDrawing = false;
		points.length = 0;
	};
    
    
function hexToRgbA(hex){
    var c;
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
        c= hex.substring(1).split('');
        if(c.length== 3){
            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c= '0x'+c.join('');
        return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+',0.3)';
    }
    throw new Error('Bad Hex');
}

	function init() {
    canvas.width = 1200;
    canvas.height = 800;

    ctx.lineWidth = 1;
    ctx.lineJoin = ctx.lineCap = 'round';
    canvas.addEventListener('mousedown', onmousedown, false);
    canvas.addEventListener('mouseup', onmouseup, false);
	}

	return {
		init: init
	}

})();