const form = document.getElementById('painting-form');
const myPaintingsLink = document.getElementById('my-paintings-link');

form.addEventListener('submit', submitHandler, false);
myPaintingsLink.addEventListener('click', loadMyPaintings, false);
CANVAS.init(form);

function submitHandler (ev) {
    ev.preventDefault();
    
    const status = document.getElementById('status');
    const canvas = document.querySelector('canvas');
    const paintingTitle = form.querySelector('[name="paintingtitle"]').value;
    const data = {
        paintingTitle: encodeURI(paintingTitle),
        painting: canvas.toDataURL()
    };
    
    status.textContent = 'Saving...';
    
    ajax('POST', form.action, data).then(function(response) {
        status.textContent = response;
    });
    
}

function loadMyPaintings (ev) {
    ev.preventDefault();
    const myPaintingsCont = document.getElementById('my-paintings-container');
    
    ajax('GET', ev.target.href).then(function(response) {
        let paintings = JSON.parse(response);
        let paintingsHTML = paintings.map((painting) => `<img src="${painting.src}" />`).join('');
        
        console.log(paintings);
        myPaintingsCont.innerHTML = paintingsHTML;
    });
}