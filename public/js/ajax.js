function ajax (method, src, params) {
    const xmlhttp = new XMLHttpRequest(); 
    params = params || '';
    return new Promise((resolve, reject) => {
        
        xmlhttp.onreadystatechange = function() {
            if(this.readyState === 4 && this.status === 200) {
                resolve(this.responseText);
            }
        };
       
        if (method === 'POST') {
            xmlhttp.open(method, src, true);
            xmlhttp.setRequestHeader('content-type', 'application/json');
            xmlhttp.send(JSON.stringify(params));
            return;
        } 
        
        xmlhttp.open(method, src + params, true);    
        xmlhttp.send();

    });
}