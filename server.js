const fs = require('fs');

module.exports = function(req, res) {
    
    let url = req.url;
    
    if (url === '/') {
        res.writeHead(200, {'content-type': 'text/html'});
        fs.createReadStream(`${__dirname}/index.html`, 'utf8').pipe(res);
    }
    
    if (url.indexOf('assets') !== -1) {
        var fileName = url.replace(/\/assets\/(\w)/, '$1');
        fs.createReadStream(`${__dirname}/public/${fileName}`, 'utf8').pipe(res);
    }
    
    if (url === '/save-painting') {
        let postData = '';
        
        req.on('data', (chunk) => {
           postData += chunk.toString('utf8'); 
        });
        
        req.on('end', () => {
            const { paintingTitle, painting } = JSON.parse(postData);
            const data = painting.split(',')[1];
    
            fs.appendFile(`${__dirname}/paintings/${paintingTitle}.png`, data, 'base64', (err) => {
                res.writeHead(200, {'content-type': 'text/plain'});
                res.end(`Painting saved as ${paintingTitle}.png`);
            });
        })
    }
    
    if (url === '/my-paintings') {
        fs.readdir(`${__dirname}/paintings`, (err, files) => {
            let paintings = files.map((file) => ({src: `/paintings/${file}`}));
            res.writeHead(200, {'content-type': 'application/json', 'charset': 'utf-8'});
            res.end(JSON.stringify(paintings));
        });
    }
    
    if (url.indexOf('/paintings') !== -1) {
        const fileName = url.replace(/\/paintings\/(\w)/, '$1');
        
        res.writeHead(200, {'content-type': 'image/png'});
        fs.createReadStream(`${__dirname}/paintings/${fileName}`).pipe(res);
    }
    
}